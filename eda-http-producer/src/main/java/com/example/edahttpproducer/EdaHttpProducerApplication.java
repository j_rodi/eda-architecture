package com.example.edahttpproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EdaHttpProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdaHttpProducerApplication.class, args);
	}

}
